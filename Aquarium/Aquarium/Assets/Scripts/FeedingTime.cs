﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedingTime : MonoBehaviour
{

    Vector3 mousePos;
    public GameObject food;
    public Bounds bounds;
    float maxX;
    float minX;
    float maxY;
    float minY;

    /// <summary>
    /// How many pieces of Food are allowed in the scene
    /// </summary>
    public int maxFood = 200;
    public static int foodCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
        maxX = bounds.maxX;
        minX = bounds.minX;
        maxY = bounds.maxY;
        minY = bounds.minY;

    }

    // Update is called once per frame
    void Update()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) + new Vector3 (0,0,10);

        // Debug.Log(mousePos);

        if(Input.GetKey(KeyCode.Mouse0) && mousePos.x >= minX && mousePos.x <= maxX && mousePos.y >= minY && mousePos.y <= maxY && foodCount <= maxFood)
        {
            Instantiate(food, mousePos, Quaternion.identity);
            foodCount++;
            // Debug.Log("foodCount: " + foodCount + ", maxFood:" + maxFood);
        }
    }
}
