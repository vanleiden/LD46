﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    public Spawn[] fishiesArray;
    public Bounds bounds;

    float boundsMinX;
    float boundsMaxX;
    float boundsMinY;
    float boundsMaxY;

    // Start is called before the first frame update
    void Start()
    {
        boundsMinX = bounds.minX;
        boundsMaxX = bounds.maxX;
        boundsMinY = bounds.minY;
        boundsMaxY = bounds.maxY;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space)) 
        {SpawnRandomFish();}
    }

    public void SpawnRandomFish()
    {
        int i = Random.Range(0,100);

        for (int j = 0; j < fishiesArray.Length; j++)
        {
            if(i >= fishiesArray[j].minProbability && i <= fishiesArray[j].maxProbability)
            {
                Instantiate(fishiesArray[j].spawnObject, new Vector3(Random.Range(boundsMinX,boundsMaxX),Random.Range(boundsMinY,boundsMaxY),0), Quaternion.Euler(0,0,-90));
                break;
            }
        }
    }

    [System.Serializable]
    public class Spawn
    {
        public GameObject spawnObject;
        public Fishies spawnFishie;
        public int minProbability = 0;
        public int maxProbability = 0;
    }
}
