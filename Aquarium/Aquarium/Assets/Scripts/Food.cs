﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public Bounds bounds;
    /// <summary>
    /// How many pieces of Food are allowed in the scene
    /// </summary>

    float minX;
    float maxX;
    float minY;
    float maxY;

    // Start is called before the first frame update
    void Start()
    {
        minX = bounds.minX;
        maxX = bounds.maxX;
        minY = bounds.minY;
        maxY = bounds.maxY;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x <= minX || transform.position.x >= maxX || transform.position.y <= minY || transform.position.y >= maxY)
        {
            Destroy(gameObject);
            FeedingTime.foodCount--;            
        }   

        transform.position += new Vector3 (Random.Range(-0.005f,0.005f),0,0);

    }

        


}
