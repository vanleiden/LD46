﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fishie : MonoBehaviour
{
    public Fishies fishie;
    float minX;
    float maxX;
    float minY;
    float maxY;

    float speed;
    int reproThreshold;
    int reproductionRate;
    float adultSize;
    float babySize;
    int minProbability;
    int maxProbability;

    private SpriteRenderer mySpriteRenderer;


    // Start is called before the first frame update
    void Start()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        minX = fishie.minX;
        minY = fishie.minY;
        maxX = fishie.maxX;
        maxY = fishie.maxY;

        speed = fishie.speed;
        reproThreshold = fishie.reproThreshold;
        reproductionRate = fishie.reproductionRate;
        adultSize = fishie.adultSize;
        babySize = fishie.babySize;
        minProbability = fishie.minProbability;
        maxProbability = fishie.maxProbability;
    }

    // Update is called once per frame
    void Update()
    {

        transform.position += new Vector3 (0.1f,0,0)*speed;

        if(transform.position.x >= maxX)
        {
            ChangeDirection();
        }

        if (transform.position.x <= minX)
        {
            ChangeDirection();
        }
        
    }

    void ChangeDirection()
    {
        mySpriteRenderer.flipY = !mySpriteRenderer.flipY;
        speed *= -1;
    }

}
