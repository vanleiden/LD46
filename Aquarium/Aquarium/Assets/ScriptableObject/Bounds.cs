﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bounds", menuName ="New Bounds")]
public class Bounds : ScriptableObject
{

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

}
