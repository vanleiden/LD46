﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Fishie", menuName = "New Fish")]

public class Fishies : ScriptableObject
{
    public new string name;
    public float speed;
    /// <summary>
    /// How many fish are needed to reproduce
    /// </summary>
    public int reproThreshold;
    /// <summary>
    /// How often do new fish spawn
    /// </summary>
    public int reproductionRate;
    /// <summary>
    /// How large is the full grown fish
    /// </summary>
    public float adultSize;
    /// <summary>
    /// How small is a newborn fish
    /// </summary>
    public float babySize;

    /// <summary>
    /// How often does this fishie randomly spawn
    /// </summary>
    public int minProbability;
    public int maxProbability;


    /// <summary>
    /// Aquarium bounds
    /// </summary>
    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

}
