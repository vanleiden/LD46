﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Log", menuName = "Log")]
public class LogSO : ScriptableObject
{
    public string text;

}
