﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickableObject : ScriptableObject
{
    public new string name;
    public GameObject clickable;

    void Click()
    {
        Destroy(clickable);
        Debug.Log(name + " clicked.");
    }

}
