﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Log : MonoBehaviour
{
    public LogSO log;
    public Text logText;
    public static Log instance;

    void Start()
    {
        instance = this;
    }
    
 
    // Update is called once per frame
    public void UpdateLog()
    {
        logText.text += "\n" + log.text;
    }
}
