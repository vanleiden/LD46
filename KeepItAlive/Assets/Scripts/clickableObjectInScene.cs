﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class clickableObjectInScene : MonoBehaviour

{

    public LogSO log;


    void OnMouseDown()
    {    
        Debug.Log(this.gameObject.name + " clicked.");
        log.text = this.gameObject.name.ToString() + " clicked.";
        Log.instance.UpdateLog();        
    }

}
